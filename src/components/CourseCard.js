import { Card, Button } from 'react-bootstrap';
import {useState} from 'react'
import PropTypes from 'prop-types';


export default function CourseCard(props){

	// Checks to see if the data was successfully passed
	//console.log(props);
	// Every component recieves information in a form of an object.
	//console.log(typeof props);

	// use the state hook for this component to be able to store its state
	// States are used to keep track of information related to  individual components

	// Syntax
		// const [getter, setter ] = useState(initalGetterValue);

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	//Using the state hook returns an array with first element being a value and the second elements as a function that's used to change the value of the first element.

	console.log(useState(0));

	function enroll() {
		if(seats > 0){
		setCount(count + 1);
		console.log('Enrollees ' + count);
     
       // Activity
		setSeats(seats -1)
		console.log('Seats: ' + seats)

		} else {
			alert("No more seats Available!")
		}
	}
	return (
		<Card>
			<Card.Body>
				<Card.Title>{props.courseProp.name}</Card.Title>
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{props.courseProp.description}</Card.Text>
				<Card.Subtitle>Price</Card.Subtitle>
				<Card.Text>{props.courseProp.price}</Card.Text>
				<Card.Text>Enrollees: {count}</Card.Text>
				<Card.Text>Seats: {seats}</Card.Text>
				<Button variant="primary" onClick={enroll}>Enroll</Button>
			</Card.Body>
		</Card>
	)
}

// Check if the CourseCard component is getting the correc prop types
// PropTypes are used for validation information passed to a components and is a tool 

CourseCard.propTypes = {
	course: PropTypes.shape({

		// Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired

	})
}