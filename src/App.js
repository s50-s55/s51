import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'
import Courses from './pages/Courses'
import './App.css';

import {Container} from 'react-bootstrap';

function App() {
  return (

  <fragment>
    <AppNavbar />
      <Container>
        <Home />
        <Courses />
      </Container>
  </fragment>

  );
}

export default App;
